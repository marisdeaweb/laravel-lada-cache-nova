<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')
          ->insert([
              'name' => 'Lamborghini Aventador',
              'created_at' => now(),
              'updated_at' => now(),
          ]);

        DB::table('car_user')
          ->insert([
              'car_id' => 1,
              'user_id' => 1,
              'available' => 1,
          ]);
    }
}
