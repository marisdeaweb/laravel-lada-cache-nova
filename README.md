laravel-lada-cache-nova

### Start

Run `composer install && cp .env.example .env && php artisan key:generate` 

then configure your db on `.env` and run `php artisan migrate && php artisan nova:user`

so configure your admin user to access to Nova and finish running `php artisan db:seed`

### How to reproduce the issue

1. Access on backend through `/nova` route
2. Go to `/nova/resources/users/1/edit-attached/cars/1?viaRelationship=cars`
3. Change the value of `Available` field and click on `Update & Continue Editing`
4. You will continue to see the old checkbox value but on db the change was correctly done
5. To see the right checkbox value from backend you need to disable or flush lada-cache
